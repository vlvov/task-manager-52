package ru.t1.vlvov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDTO extends AbstractUserOwnedModelDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = "project_id")
    private String projectId = null;

    @NotNull
    @Column
    private String name = "";

    @NotNull
    @Column
    private String description = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column
    private Date created = new Date();

}
