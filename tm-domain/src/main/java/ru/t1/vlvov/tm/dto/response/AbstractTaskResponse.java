package ru.t1.vlvov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    public AbstractTaskResponse(TaskDTO task) {
        this.task = task;
    }

    @Nullable
    private TaskDTO task;

}
