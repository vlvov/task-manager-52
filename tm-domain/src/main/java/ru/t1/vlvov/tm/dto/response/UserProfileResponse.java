package ru.t1.vlvov.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.t1.vlvov.tm.dto.model.UserDTO;

@NoArgsConstructor
public class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(UserDTO user) {
        super(user);
    }

}
