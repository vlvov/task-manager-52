package ru.t1.vlvov.tm.api.service.dto;

import ru.t1.vlvov.tm.dto.model.SessionDTO;

public interface ISessionServiceDTO extends IUserOwnedServiceDTO<SessionDTO> {
}
