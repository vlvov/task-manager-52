package ru.t1.vlvov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.repository.dto.IRepositoryDTO;
import ru.t1.vlvov.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractRepositoryDTO<M extends AbstractModelDTO> implements IRepositoryDTO<M> {

    @NotNull
    final protected EntityManager entityManager;

    public AbstractRepositoryDTO(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void set(@NotNull final Collection<M> models) {
        clear();
        models.forEach(this::add);
    }

}
