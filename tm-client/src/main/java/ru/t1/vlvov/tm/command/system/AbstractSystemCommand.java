package ru.t1.vlvov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.vlvov.tm.api.service.ICommandService;
import ru.t1.vlvov.tm.api.service.ILoggerService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.exception.system.ServiceNotFoundException;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ISystemEndpoint getSystemEndpoint() {
        if (serviceLocator == null) throw new ServiceNotFoundException();
        return serviceLocator.getSystemEndpoint();
    }

    @NotNull
    protected ICommandService getCommandService() {
        if (serviceLocator == null) throw new ServiceNotFoundException();
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected ILoggerService getLoggerService() {
        if (serviceLocator == null) throw new ServiceNotFoundException();
        return serviceLocator.getLoggerService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        if (serviceLocator == null) throw new ServiceNotFoundException();
        return serviceLocator.getPropertyService();
    }

}
